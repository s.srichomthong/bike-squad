package com.cmu.jaylerr.bikesquad.utility.sharedpreference;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedKey;

/**
 * Created by jaylerr on 08-May-17.
 */

public class AccountPreference {
    private Activity activity;
    private SharedPreferences account;
    private SharedPreferences.Editor editor;
    private String username;
    private String password;

    public AccountPreference(Context context){
        this.activity = (Activity) context;
        account = activity.getSharedPreferences(SharedKey._key_setting, Context.MODE_PRIVATE);
        editor = account.edit();
    }
}
