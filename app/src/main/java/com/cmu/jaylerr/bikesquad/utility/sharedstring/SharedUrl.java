package com.cmu.jaylerr.bikesquad.utility.sharedstring;

/**
 * Created by jaylerr on 02-Apr-17.
 */

public class SharedUrl {
    public static String url_base =                                         "http://www.srichomthong.me/";
    public static String url_server =                                       "app/connection/connection.php";

    public static String url_register =                                     "public/register";
    public static String url_sign_in =                                      "public/sign_in";

    public static String url_change_password =                              "members/change_password";
    public static String url_activity_new =                                 "activity/new";
    public static String url_activity_remove =                              "activity/remove";
    public static String url_activity_edit =                                "activity/edit";
}
