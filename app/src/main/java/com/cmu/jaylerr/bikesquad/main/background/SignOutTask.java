package com.cmu.jaylerr.bikesquad.main.background;

import android.app.Activity;
import android.content.Intent;

import com.cmu.jaylerr.bikesquad.splash.views.SplashActivity;
import com.cmu.jaylerr.bikesquad.utility.sharedpreference.StatePreference;

/**
 * Created by jaylerr on 09-May-17.
 */

public class SignOutTask {
    Activity activity;

    public SignOutTask(Activity activity) {
        this.activity = activity;
    }

    public void signOut(){
        StatePreference statePreference = new StatePreference(activity);
        statePreference.setUserState(false);

        Intent intent = new Intent(activity, SplashActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }
}
