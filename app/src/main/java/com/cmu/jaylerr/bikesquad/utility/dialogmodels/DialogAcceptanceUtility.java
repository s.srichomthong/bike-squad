package com.cmu.jaylerr.bikesquad.utility.dialogmodels;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.cmu.jaylerr.bikesquad.R;
import com.cmu.jaylerr.bikesquad.main.background.SignOutTask;
import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedFlag;

/**
 * Created by jaylerr on 16-Feb-17.
 */

public class DialogAcceptanceUtility extends Dialog implements View.OnClickListener {

    public Activity activity;
    private Button yes, no;
    private String title, message, target;
    private TextView messageView;
    private TextView titleView;
    private View view;

    public DialogAcceptanceUtility(Activity activity, String title, String message, String target) {
        super(activity);
        this.activity = activity;
        this.title = title;
        this.message = message;
        this.target = target;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_decide);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        titleView = (TextView) findViewById(R.id.txt_title);
        messageView = (TextView) findViewById(R.id.txt_message);
        titleView.setText(title);
        messageView.setText(message);
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        view = v;
        if (v.getId()==R.id.btn_yes){
            targetAcceptTask();
        }else dismiss();
    }

    private void targetAcceptTask(){
        if (target.equals(SharedFlag._flag_sign_out)){
            SignOutTask signOutTask = new SignOutTask(activity);
            signOutTask.signOut();
        } else if (target.equals(SharedFlag._flag_on_back_press)){
            activity.finish();
        }else if(target.equals(SharedFlag._flag_error_connection)){

        }
    }
}
