package com.cmu.jaylerr.bikesquad.models.modelsrequest;

/**
 * Created by jaylerr on 04-May-17.
 */

public class RegisterRequest {
    String email;
    String password;
    String repassword;
    String firstname;
    String lastname;

    public RegisterRequest() {
    }

    public RegisterRequest(String email, String password, String repassword, String firstname, String lastname) {
        this.email = email;
        this.password = password;
        this.repassword = repassword;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepassword() {
        return repassword;
    }

    public void setRepassword(String repassword) {
        this.repassword = repassword;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
