package com.cmu.jaylerr.bikesquad.utility.sharedstring;

/**
 * Created by jaylerr on 02-Apr-17.
 */

public class SharedKey {
    public static String _key_state =                       "STATE";
    public static String _key_state_signed_in =             "SIGNED_IN";
    public static String _key_state_username =              "USERNAME_STATE";
    public static String _key_state_password =              "PASSWORD_STATE";

    public static String _key_setting =                     "SETTING";
    public static String _key_setting_theme =               "SETTING_THEME";
    public static String _key_setting_language =            "SETTING_LANGUAGE";
    public static String _key_setting_save_data =           "SETTING_SAVE_DATA";

    public static String _key_putExtra_member_to_main =     "MEMBERS2MAIN";
}
