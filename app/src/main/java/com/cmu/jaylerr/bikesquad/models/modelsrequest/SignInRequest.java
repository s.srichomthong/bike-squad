package com.cmu.jaylerr.bikesquad.models.modelsrequest;

/**
 * Created by jaylerr on 08-May-17.
 */

public class SignInRequest {
    String username;
    String password;

    public SignInRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
