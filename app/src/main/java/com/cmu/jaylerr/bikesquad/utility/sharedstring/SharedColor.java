package com.cmu.jaylerr.bikesquad.utility.sharedstring;

/**
 * Created by jaylerr on 09-May-17.
 */

public class SharedColor {
    public static String _color_splash_hite =                        "#f0f2f2";
    public static String _color_primary_green =                      "#63AD32";
    public static String _color_red =                                "#FF0000";
    public static String _color_deep_orange =                       "#FF4000";
    public static String _color_orange =                            "#FF8000";
    public static String _color_deep_yellow =                       "#FFBF00";
    public static String _color_yellow =                            "#FFFF00";
    public static String _color_deep_light_green =                  "#80FF00";
    public static String _color_light_green =                       "#40FF00";
    public static String _color_green =                             "#00FF00";
    public static String _color_deep_green =                        "#088A08";
    public static String _color_blue_sky =                          "#00FFFF";
    public static String _color_blue =                              "#0000FF";
    public static String _color_deep_blue =                         "#08088A";
    public static String _color_purple =                            "#6A0888";
    public static String _color_violet =                            "#8904B1";
    public static String _color_deeppink =                          "#DF0174";
    public static String _color_pink =                              "#FF00FF";
    public static String _color_light_pink =                        "#F781F3";
    public static String _color_black =                             "#000000";
    public static String _color_white =                             "#FFFFFF";
    public static String _color_gray =                              "#424242";
    public static String _color_silver =                            "#D8D8D8";
    public static String _color_light_gray =                        "#e6e6e6";
    public static String _color_silver_gray =                       "#cccccc";
}
