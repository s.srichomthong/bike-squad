package com.cmu.jaylerr.bikesquad.authentication.views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.cmu.jaylerr.bikesquad.R;
import com.cmu.jaylerr.bikesquad.authentication.background.MemberValidator;
import com.cmu.jaylerr.bikesquad.authentication.background.RegisterTask;
import com.cmu.jaylerr.bikesquad.models.modelsrequest.RegisterRequest;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {
    View view;
    private RegisterRequest registerRequest;
    @BindView(R.id.edit_reg_email) EditText email;
    @BindView(R.id.edit_reg_password) EditText password;
    @BindView(R.id.edit_reg_re_password) EditText repassword;
    @BindView(R.id.edit_reg_first_name) EditText firstname;
    @BindView(R.id.edit_reg_last_name) EditText lastname;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        registerRequest = new RegisterRequest();
        return view;
    }

    @OnClick(R.id.button_reg_register)
    public void OnRegister(){
        registerRequest.setEmail(email.getText().toString());
        registerRequest.setPassword(password.getText().toString());
        registerRequest.setRepassword(repassword.getText().toString());
        registerRequest.setFirstname(firstname.getText().toString());
        registerRequest.setLastname(lastname.getText().toString());
        MemberValidator memberValidator = new MemberValidator(getActivity(), registerRequest );
        if (memberValidator.isRequestForm()){
            RegisterTask registerTask = new RegisterTask();
        }

    }

    private Boolean isEmail(){
        return false;
    }

    @OnClick(R.id.text_reg_back)
    public void OnBack(){
        SignInFragment signInFragment = new SignInFragment();
        FragmentManager manager = getFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.replace(R.id.frame_fragment_content_control,signInFragment);
        transaction.commit();
    }
}
