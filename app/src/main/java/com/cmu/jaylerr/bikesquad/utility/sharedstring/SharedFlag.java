package com.cmu.jaylerr.bikesquad.utility.sharedstring;

/**
 * Created by jaylerr on 08-May-17.
 */

public class SharedFlag {
    public static String _flag_restart_app                          = "RESTART_APP";
    public static String _flag_do_nothing                           = "NOTHING";
    public static String _flag_finish_activity                      = "FINISH_ACTIVITY";

    public static String _flag_sign_out                             = "SIGN_OUT";
    public static String _flag_on_back_press                        = "BACK_PRESS";
    public static String _flag_error_connection                     = "ERROR_CONNECTION";
}
