package com.cmu.jaylerr.bikesquad.authentication.background;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Parcelable;

import com.cmu.jaylerr.bikesquad.R;
import com.cmu.jaylerr.bikesquad.main.views.MainActivity;
import com.cmu.jaylerr.bikesquad.models.Members;
import com.cmu.jaylerr.bikesquad.models.modelsrequest.SignInRequest;
import com.cmu.jaylerr.bikesquad.utility.sharedpreference.StatePreference;
import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedKey;

import org.parceler.Parcels;

/**
 * Created by jaylerr on 08-May-17.
 */

public class SignInTask extends AsyncTask <String, Void, String> {
    private Activity activity;
    private String username;
    private String password;
    private SignInRequest signInRequest;
    private Members members;

    public SignInTask(Activity activity, String username, String password) {
        this.activity = activity;
        this.username = username;
        this.password = password;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        signInRequest = new SignInRequest(username, password);
        signInRequest.setUsername(username);
        signInRequest.setPassword(password);
    }

    @Override
    protected String doInBackground(String... params) {
//        try {
//            ServiceSignIn serviceSignIn = ServiceBase.getRetrofit().create(ServiceSignIn.class);
//            Call <Members> call = serviceSignIn.getSignIn(signInRequest);
//            call.enqueue(new Callback<Members>() {
//
//                @Override
//                public void onResponse(Call <Members> call, Response<Members> response) {
//                    if (response.isSuccessful()) {
//                        try {
//                            members = response.body();
//                            onSuccessfully();
//                        }catch (Exception e){
//                            e.printStackTrace();
//                        }
//                    }else{
//
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<Members> call, Throwable t) {
//                    t.printStackTrace();
//                    dialogOnFailure(activity.getString(R.string.authen_sign_in_false));
//                }
//            });
//        }catch (Exception e){
//            e.printStackTrace();
//            dialogOnFailure(activity.getString(R.string.app_message_something_wrong));
//        }

        fackUser();
        onSuccessfully();
        return null;
    }

    private void setSharedPreference(){
        StatePreference statePreference = new StatePreference(activity);
        statePreference.setUserState(true);
        statePreference.setUserInformation(username, password);
    }

    private void onSuccessfully(){
        if(members != null){
            setSharedPreference();
            Parcelable wrapped = Parcels.wrap(members);
            Intent intent = new Intent(activity, MainActivity.class);
            intent.putExtra(SharedKey._key_putExtra_member_to_main, wrapped);
            activity.startActivity(intent);
            activity.finish();
        }else dialogOnFailure(activity.getString(R.string.authen_sign_in_false));
    }

    private void dialogOnFailure(final String string){
        setSharedPreference();
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
        activity.finish();
//        activity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                DialogWarningMessageUtility dialogWMU = new DialogWarningMessageUtility(
//                        activity, activity.getString(R.string.dialog_title_message),
//                        string, SharedFlag._flag_do_nothing);
//                dialogWMU.show();
//            }
//        });
    }

    private void fackUser(){
        members = new Members();
        members.setUsername("Mypandacm");
        members.setEmail("Mypandacm@htmail.co.th");
        members.setFirstname("Sapthawee");
        members.setLastname("Srichomthong");
        members.setBirthdate("25-11-2537");
        members.setRegisterdate("1-1-2015");
        members.setImage_url("http://ikf.gujarattourism.com/asset/images/user.png");
    }
}
