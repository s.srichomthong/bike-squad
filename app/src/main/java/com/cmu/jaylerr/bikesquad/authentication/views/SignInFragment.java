package com.cmu.jaylerr.bikesquad.authentication.views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.cmu.jaylerr.bikesquad.R;
import com.cmu.jaylerr.bikesquad.authentication.background.SignInTask;
import com.cmu.jaylerr.bikesquad.utility.dialogmodels.DialogWarningMessageUtility;
import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedFlag;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends Fragment {
    View view;
    @BindView(R.id.edit_sign_username) EditText editText_username;
    @BindView(R.id.edit_sign_password) EditText editText_password;

    private String username;
    private String password;

    public SignInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        ButterKnife.bind(this, view);
        // Inflate the layout for this fragment
        return view;
    }

    @OnClick(R.id.button_sign_in)
    public void onSignInClick(){
        username = editText_username.getText().toString().toLowerCase();
        password = editText_password.getText().toString();
        validation();
    }

    @OnClick(R.id.text_register)
    public void onRegister() {
        RegisterFragment registerFragment = new RegisterFragment();
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        transaction.replace(R.id.frame_fragment_content_control,registerFragment);
        transaction.commit();
    }

    private void validation(){
        if (username.isEmpty()){
            dialogMessage(getString(R.string.authen_warning_username_require));
        }else if (password.isEmpty()){
            dialogMessage(getString(R.string.authen_warning_password_require));
        }else if (username.contains("@")){
            //use email
            if (isEmailForm(username)){
                signingIn();
            }else {
                dialogMessage(getString(R.string.authen_warning_email_incorrect));
            }
        }else {
            //use username
            signingIn();
        }
    }

    private void signingIn(){
        try {
            SignInTask signInTask = new SignInTask(getActivity(), username, password);
            signInTask.execute();
        }catch (Exception ex){
            ex.printStackTrace();
            dialogMessage(getString(R.string.app_message_something_wrong));
        }
    }

    private Boolean isEmailForm(String text){
        if(text.length()<5){
            return false;
        }else if (text.contains("@")){
            String []splitText;
            splitText = text.split("@");
            if (splitText[1].contains(".")){
                return true;
            }else return false;
        }
        return false;
    }

    private void dialogMessage(String string){
        DialogWarningMessageUtility dialog = new DialogWarningMessageUtility(getActivity(),
                getActivity().getString(R.string.dialog_title_warning), string, SharedFlag._flag_do_nothing);
        dialog.show();
    }
}
