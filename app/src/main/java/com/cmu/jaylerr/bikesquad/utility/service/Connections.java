package com.cmu.jaylerr.bikesquad.utility.service;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by jaylerr on 12-Mar-17.
 */

public class Connections {
    Activity activity;

    public Connections(Activity activity) {
        this.activity = activity;
    }

    public Boolean isConnected(){
        ConnectivityManager connection = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connection.getActiveNetworkInfo();
        return (networkInfo == null)? false :true ;
    }

}
