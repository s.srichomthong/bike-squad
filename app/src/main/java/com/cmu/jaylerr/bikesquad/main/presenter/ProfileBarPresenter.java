package com.cmu.jaylerr.bikesquad.main.presenter;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cmu.jaylerr.bikesquad.R;
import com.cmu.jaylerr.bikesquad.models.Members;

/**
 * Created by jaylerr on 09-May-17.
 */

public class ProfileBarPresenter {
    private Activity activity;
    private View headerLayout;
    private TextView name;
    private TextView email;
    private ImageView img_profile;
    private Members members;

    public ProfileBarPresenter(Activity activity, View headerLayout, Members members) {
        this.activity = activity;
        this.headerLayout = headerLayout;
        this.members = members;
        name = (TextView) headerLayout.findViewById(R.id.text_main_profile_full_name);
        email = (TextView) headerLayout.findViewById(R.id.text_main_profile_email);
        img_profile = (ImageView) headerLayout.findViewById(R.id.img_main_profile);
    }

    public void setProfileBarContent(){
        try{
            name.setText(members.getFirstname().concat(" ".concat(members.getLastname())));
            email.setText(members.getEmail());

            Glide.with(activity)
                    .load(members.getImage_url())
                    .fitCenter()
                    .animate(R.anim.fade_in)
                    .placeholder(R.drawable.ic_image_placeholder)
                    .error(R.drawable.ic_image_error)
                    .into(img_profile);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
