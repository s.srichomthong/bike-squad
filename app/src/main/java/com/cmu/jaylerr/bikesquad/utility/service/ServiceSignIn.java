package com.cmu.jaylerr.bikesquad.utility.service;

import com.cmu.jaylerr.bikesquad.models.Members;
import com.cmu.jaylerr.bikesquad.models.modelsrequest.SignInRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by jaylerr on 31-Jan-17.
 */

public interface ServiceSignIn {
    @POST("public/sign_in")
    Call<Members> getSignIn(@Body SignInRequest signInRequest);
}
