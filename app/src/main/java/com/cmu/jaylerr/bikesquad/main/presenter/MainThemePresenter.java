package com.cmu.jaylerr.bikesquad.main.presenter;

import android.app.Activity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.cmu.jaylerr.bikesquad.R;
import com.cmu.jaylerr.bikesquad.utility.configuration.ColorManager;
import com.cmu.jaylerr.bikesquad.utility.sharedpreference.SettingPreference;
import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedColor;

/**
 * Created by jaylerr on 09-May-17.
 */

public class MainThemePresenter {
    private Activity activity;
    private View headerLayout;
    private LinearLayout linearLayout;
    private  ColorManager colorManager;
    private Toolbar toolbar;

    public MainThemePresenter(Activity activity, View headerLayout) {
        this.activity = activity;
        this.headerLayout = headerLayout;
        linearLayout = (LinearLayout) headerLayout.findViewById(R.id.nav_profile_bar);
        colorManager = new ColorManager();
        toolbar =  (Toolbar) activity.findViewById(R.id.toolbar);
    }

    public void setProfileBarTheme(){
        SettingPreference settingPreference = new SettingPreference(activity);
        linearLayout.setBackground(colorManager.getGradientDrawable(settingPreference.getSettingTheme()));
    }

    public void setToolBar(){
        ColorManager colorManager = new ColorManager();
        toolbar.setBackground(colorManager.getGradientDrawable(SharedColor._color_primary_green));
    }
}
