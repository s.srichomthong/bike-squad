package com.cmu.jaylerr.bikesquad.utility.service;

import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedUrl;
import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedValue;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jaylerr on 31-Jan-17.
 */

public class ServiceBase {
    private static String BASE_URL;
    private static Retrofit retrofit = null;

    public static Retrofit getRetrofit(){
        BASE_URL = SharedUrl.url_base;
        if (retrofit==null){
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(SharedValue.connection_time_out, TimeUnit.SECONDS)
                    .writeTimeout(SharedValue.connection_time_out,TimeUnit.SECONDS)
                    .readTimeout(SharedValue.connection_time_out, TimeUnit.SECONDS)
                    .addInterceptor(createHttpLoggingInterceptor())
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }

    private static HttpLoggingInterceptor createHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }
}
