package com.cmu.jaylerr.bikesquad.utility.service;

import com.cmu.jaylerr.bikesquad.models.Members;
import com.cmu.jaylerr.bikesquad.models.modelsrequest.SignInRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by jaylerr on 31-Jan-17.
 */

public interface ServiceUpdateUser {
    @POST("public/sign_in")
    Call<Members> getUpdateUser(@Body SignInRequest signInRequest);
}
